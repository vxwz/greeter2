from pathlib import Path
import logging
from logging.config import fileConfig
import uvicorn
from greeter2 import api


def run_server():
    path: Path = Path(__file__)
    log_file_path: str = path.parents[1].joinpath("config/logging_config.ini")
    logging.config.fileConfig(log_file_path, disable_existing_loggers=False)
    logger = logging.getLogger()
    logger.info("Asking for FastAPI object")
    app = api.get_app()
    logger.info("Starting Uvicorn Server on port %d", 5000)
    uvicorn.run(app, host="127.0.0.1", port=5000, log_config=str(log_file_path))


if __name__ == "__main__":
    run_service()
