#================================================
# Full OS image to setup env to build application
#================================================

FROM python:3.8-slim-buster AS base

ENV LANG=C.UTF-8 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONIOENCODING=UTF-8 \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_NO_CACHE_DIR=off \
    PIP_CACHE_DIR="/opt/cache/pip" \
    OLD_PATH="${PATH}"

ENV APP_BUILD_PATH="/build" \
    APP_PATH="/app"

# WARNING! You cannot have a POETRY_HOME in application build directory \
# (APP_BUILD_PATH). It messes up poetry and causes really hard to debug 
# issue with concurrent.futures on running pytest.
ENV POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    POETRY_URL="https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py"

# Install compiler and associated libraries
RUN apt-get -y update \
    && apt-get -y --no-install-recommends install \
    curl build-essential gcc

# Download and install poetry
WORKDIR ${APP_BUILD_PATH}
RUN curl \
    --silent \
    --show-error \
    --remote-name \
    --location ${POETRY_URL} && \
    python get-poetry.py

# Dependent variables must be defined separately otherwise doesn't work.
ENV VENV_PATH="${APP_BUILD_PATH}/venv" \
    DEPLOY_VIRTUALENVS_PATH="${APP_PATH}/venv" \
    PIP_DOWNLOAD_PATH="${APP_BUILD_PATH}/pip-cache"

# Install upgraded packages for pip and setuptools
# Create blank virtual environments to aid caching
RUN mkdir -p ${PIP_DOWNLOAD_PATH} && \
    python -m venv ${VENV_PATH} && \
    python -m venv ${DEPLOY_VIRTUALENVS_PATH}

#================================================
# Install dependencies
#================================================

FROM base AS builder

COPY --from=base ${APP_BUILD_PATH} ${APP_BUILD_PATH}
COPY --from=base ${APP_PATH} ${APP_PATH}

# poetry export --without-hashes --dev --format requirements.txt -o dev-req.txt
# poetry export --without-hashes --format requirements.txt -o dep-req.txt
ENV POETRY_LOCK="poetry.lock" \
    PROJECT_SPEC="pyproject.toml" \ 
    DEVELOPMENT_REQUIREMENT="dev-req.txt" \
    DEPLOYMENT_REQUIREMENT="dep-req.txt"

WORKDIR ${APP_BUILD_PATH}
COPY ${POETRY_LOCK} \
    ${PROJECT_SPEC} \
    ${DEVELOPMENT_REQUIREMENT} \
    ${DEPLOYMENT_REQUIREMENT} \
    ./

# Switch to build virtual environment
ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${OLD_PATH}"

# Tell poetry to not create virtualenv and use exising virtual environment
RUN poetry config --local virtualenvs.create false && \
    poetry config --local virtualenvs.path ${VENV_PATH} && \
    ls -la ${APP_BUILD_PATH}

# Download to be in a separate step from install otherwise caching does not work
RUN pip download --destination-directory ${PIP_DOWNLOAD_PATH} -r ${DEVELOPMENT_REQUIREMENT}

# Install development dependencies
RUN pip install --find-links=${PIP_DOWNLOAD_PATH} -r ${DEVELOPMENT_REQUIREMENT}

# Switch to deployment virtual environment and install dependencies
ENV PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${OLD_PATH}"
RUN pip install --find-links=${PIP_DOWNLOAD_PATH} -r ${DEPLOYMENT_REQUIREMENT}

#================================================
# Package the application
#================================================

FROM builder AS package

ARG EXT_PYPI_URL
ARG EXT_PYPI_USER
ARG EXT_PYPI_PASS
ARG PACKAGE_NAME
ARG PYPI_URL

COPY --from=builder ${APP_BUILD_PATH} ${APP_BUILD_PATH}
COPY --from=builder ${APP_PATH} ${APP_PATH}

# Copy code. This respects entries in .dockerignore file
WORKDIR ${APP_BUILD_PATH}
COPY . .
COPY ./scripts/entrypoint.sh ${APP_PATH}

# Build and upload the wheel distribution
ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${OLD_PATH}"
RUN pwd && ls -la ${APP_BUILD_PATH} && ls -la ${APP_PATH} && \
    poetry config --local repositories.ext_pypi ${EXT_PYPI_URL} && \
    poetry build --format wheel && \
    poetry publish --repository ext_pypi --username ${EXT_PYPI_USER} --password ${EXT_PYPI_PASS}

# Install the wheel distribution that has just been uploaded
ENV PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${OLD_PATH}"
RUN chmod +x ${APP_PATH}/entrypoint.sh && \
    pip install \
    --find-links=${PIP_DOWNLOAD_PATH} \
    --extra-index-url ${PYPI_URL} \
    ${PACKAGE_NAME} 

#================================================
# Build a smaller deployment image
#================================================

FROM python:3.8-slim-buster AS deployment

# Variables are not copied if the base image is different
ENV LANG=C.UTF-8 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIP_NO_CACHE_DIR=off \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    APP_PATH="/app" \
    GUNICORN_CONFIG="config/gunicorn_config.py" \
    LOGGING_CONFIG="config/logging_config.ini"

ENV DEPLOY_VIRTUALENVS_PATH="${APP_PATH}/venv"
ENV PATH="${DEPLOY_VIRTUALENVS_PATH}/bin:${PATH}"

COPY --from=package ${APP_PATH} ${APP_PATH} 

# Pass the name of variables since CMD will not perform substitution
# However it does pass the environment as it is so we can resolve the
# variable within bash script
WORKDIR ${APP_PATH}
EXPOSE ${APP_PORT}
CMD ["./entrypoint.sh", "DEPLOY_VIRTUALENVS_PATH", "GUNICORN_CONFIG", "LOGGING_CONFIG"]
