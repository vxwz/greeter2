import logging
from typing import Dict
from fastapi import FastAPI
from greeter2 import core

app: FastAPI = FastAPI()


def get_app() -> FastAPI:
    return app


@app.get("/")
async def greet() -> Dict[str, str]:
    logger = logging.getLogger(__name__)
    logger.info("Endpoint: /")
    return {"message": core.greeting()}
