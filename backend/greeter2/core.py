import logging


def greeting() -> str:
    logger = logging.getLogger(__name__)
    logger.info("Generating a greeting for the world")
    return "Hello, World!"
