#!/bin/bash

PARAM_1=$1
PARAM_2=$2
PARAM_3=$3

VIRTUALENV_PATH=${!PARAM_1}
GUNICORN_CONFIG=${!PARAM_2}
LOGGING_CONFIG=${!PARAM_3}
CHECK_CONFIG=1
WORKER_CLASS="uvicorn.workers.UvicornWorker"

# If a command fails, raise exception.
echo "Current Directory: `pwd` "
echo "Contents of Current Directory: `ls -la`"
echo "Virtual Env Path: ${VIRTUALENV_PATH}"
echo "Application: ${APPLICATION}"

# Activate the virtual environment
. ${VIRTUALENV_PATH}/bin/activate

PATH_TO_CONFIG=$( python -c 'import site; print(site.getsitepackages()[0])' )
GUNICORN_CONFIG="file:${PATH_TO_CONFIG}/${GUNICORN_CONFIG}"
LOGGING_CONFIG="${PATH_TO_CONFIG}/${LOGGING_CONFIG}"

echo "Path to config directory: ${PATH_TO_CONFIG}"
echo "Gunicorn config: ${GUNICORN_CONFIG}"
echo "Logging config: ${LOGGING_CONFIG}"

set -e

# Evaluating passed command
gunicorn  \
    --worker-class ${WORKER_CLASS}  \
    --config ${GUNICORN_CONFIG}     \
    --log-config ${LOGGING_CONFIG}  \
    ${APPLICATION}
