#=================================================
# Exit immediately if any line throws error
#=================================================
set -e

source scripts/run_coverage.sh
source scripts/delete_package.sh

#=================================================
# Parameters
#=================================================
CURR_DIR="."
PROJECT_ID="22848494"
PACKAGE_NAME="greeter2"
DOCKERFILE=backend.dockerfile
APP_OBJECT="${PROJECT_NAME}.api:app"
REGISTRY_USER="gitlab+deploy-token-295742"
REGISTRY_PASS="GydopSv-mTarQjjpYQ7t"
DOCKER_REGISTRY="localhost:5000"
USE_REMOTE_REGISTRY=0
PYPI_UPLOAD_URL="https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/pypi"
PYPI_DOWNLOAD_URL="https://${REGISTRY_USER}:${REGISTRY_PASS}@gitlab.com/api/v4/projects/${PROJECT_ID}/packages/pypi/simple"

#=================================================
# Allow user to override parameters
#=================================================
for i in "$@"
do
case $i in
    -u=*|--registry_user=*)
    REGISTRY_USER="${i#*=}"
    shift # past argument=value
    ;;
    -p=*|--registry_pass=*)
    REGISTRY_PASS="${i#*=}"
    shift # past argument=value
    ;;
    -r=*|--registry=*)
    DOCKER_REGISTRY="${i#*=}"
    shift # past argument=value
    ;;
    -n=*|--upload_url=*)
    PYPI_UPLOAD_URL="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--download_url=*)
    PYPI_DOWNLOAD_URL="${i#*=}"
    shift # past argument=value
    ;;
    -a=*|--package_name=*)
    PACKAGE_NAME="${i#*=}"
    shift # past argument=value
    ;;
    -o=*|--app_object=*)
    APP_OBJECT="${i#*=}"
    shift # past argument=value
    ;;
    -i=*|--USE_REMOTE_REGISTRY=*)
    USE_REMOTE_REGISTRY="${i#*=}"
    shift # past argument=value
    ;;
    *)    # unknown option
    ;;
esac
done

#=================================================
# Define the tags
#=================================================
BASE_TAG="${DOCKER_REGISTRY}/base"
PACKAGE_TAG="${DOCKER_REGISTRY}/package"
BUILDER_TAG="${DOCKER_REGISTRY}/builder"
DEPLOYMENT_TAG="${DOCKER_REGISTRY}/greeter"

#=================================================
# Debug information
#=================================================

echo "==============================================="
echo "Parameters:"
echo "==============================================="
echo "Project = ${PROJECT_ID}"
echo "Registry = ${DOCKER_REGISTRY}"
echo "Registry User = ${REGISTRY_USER}"
echo "Push Images: ${USE_REMOTE_REGISTRY}"
echo "Base image tag: ${BASE_TAG}"
echo "Package image tag: ${PACKAGE_TAG}"
echo "Builder image tag: ${BUILDER_TAG}"
echo "Deployment image tag: ${DEPLOYMENT_TAG}"
echo "PyPI Upload URL = ${PYPI_UPLOAD_URL}"
echo "PyPI Download URL = ${PYPI_DOWNLOAD_URL}"
echo "==============================================="

#=================================================
# Pull images
#=================================================
if [[ ${USE_REMOTE_REGISTRY} -eq 1 ]]; then
    docker image pull ${BASE_TAG}
    docker image pull ${BUILDER_TAG}
    docker image pull ${PACKAGE_TAG}
    docker image pull ${DEPLOYMENT_TAG}
fi

#=================================================
# Build images
#=================================================

echo "================================================="
echo " Build the base image"
echo "================================================="

docker build \
    --file ${DOCKERFILE} \
    --target base \
    --tag "${BASE_TAG}" \
    --cache-from "${BASE_TAG}" \
    ${CURR_DIR}

if [[ ${USE_REMOTE_REGISTRY} -eq 1 ]]; then
    docker image push ${BASE_TAG}
fi

echo "================================================="
echo " Build the builder image"
echo "================================================="

docker build \
    --file ${DOCKERFILE} \
    --target builder \
    --tag "${BUILDER_TAG}" \
    --cache-from "${BASE_TAG}" \
    --cache-from "${BUILDER_TAG}" \
    ${CURR_DIR} 

if [[ ${USE_REMOTE_REGISTRY} -eq 1 ]]; then
    docker image push ${BUILDER_TAG}
fi

echo "================================================="
echo " Build the package image"
echo "================================================="

docker build \
    --file ${DOCKERFILE} \
    --target package \
    --build-arg EXT_PYPI_URL="${PYPI_UPLOAD_URL}" \
    --build-arg EXT_PYPI_USER="${REGISTRY_USER}" \
    --build-arg EXT_PYPI_PASS="${REGISTRY_PASS}" \
    --build-arg PYPI_URL="${PYPI_DOWNLOAD_URL}" \
    --build-arg PACKAGE_NAME="${PACKAGE_NAME}" \
    --tag "${PACKAGE_TAG}" \
    --cache-from "${BUILDER_TAG}"  \
    --cache-from "${BASE_TAG}" \
    --cache-from "${PACKAGE_TAG}"  \
    ${CURR_DIR} 

if [[ ${USE_REMOTE_REGISTRY} -eq 1 ]]; then
    docker image push ${PACKAGE_TAG}
fi

echo "================================================="
echo " Build the deployment image"
echo "================================================="

docker build \
    --file ${DOCKERFILE} \
    --target deployment \
    --build-arg EXT_PYPI_URL="${PYPI_UPLOAD_URL}" \
    --build-arg EXT_PYPI_USER="${REGISTRY_USER}" \
    --build-arg EXT_PYPI_PASS="${REGISTRY_PASS}" \
    --build-arg PYPI_URL="${PYPI_DOWNLOAD_URL}" \
    --build-arg PACKAGE_NAME="${PACKAGE_NAME}" \
    --build-arg APP_OBJECT="${APP_OBJECT}" \
    --tag "${DEPLOYMENT_TAG}" \
    --cache-from "${PACKAGE_TAG}"  \
    --cache-from "${BUILDER_TAG}"  \
    --cache-from "${BASE_TAG}" \
    --cache-from "${DEPLOYMENT_TAG}" \
    ${CURR_DIR}

if [[ ${USE_REMOTE_REGISTRY} -eq 1 ]]; then
    docker image push ${DEPLOYMENT_TAG}
fi
