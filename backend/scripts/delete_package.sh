#=================================================
# Exit immediately if any line throws error
#=================================================
set -e

#=================================================
# Pypi does not allow overwriting so delete existing
#=================================================
PROJECT_ID="22848494"
PRIVATE_TOKEN="xRgh-QxFRv2-zjBos6nP"

# Get all packages within the repository
CONTENT=$( curl --header PRIVATE-TOKEN:${PRIVATE_TOKEN} --silent --show-error --location https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/ )

# Parse using jq and isolate the package Id. 
# TODO: Assumption is being made that we have only one package.
if [[ ! -z "$CONTENT" ]]; then
    echo "${CONTENT}" | jq '.[] | {id: .id, name: .name, created: .created_at, version: .version, type: .package_type}'
    PACKAGE_ID=$(jq -r '.[] | .id' <<< "${CONTENT}")
fi

if [[ ${PACKAGE_ID} -gt 0 ]]; then
    echo "Deleting package with Id: ${PACKAGE_ID}"    
    RESULT=$( curl --silent --show-error --request DELETE --header PRIVATE-TOKEN:${PRIVATE_TOKEN} --location  https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/${PACKAGE_ID} )
fi
