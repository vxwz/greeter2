#=================================================
# Exit immediately if any line throws error
#=================================================
set -e

#=================================================
# Run tests first to discover build failure early
#=================================================
PATH_TESTS="./tests"
PATH_SPEC="./pyproject.toml"
COVERAGE_THRESHOLD=90
coverage run --rcfile ${PATH_SPEC} -m pytest ${PATH_TESTS}
coverage report --fail-under ${COVERAGE_THRESHOLD}
