import os
import json
import multiprocessing

NUM_CORES = multiprocessing.cpu_count()
WORKER_PER_CORE = int(os.getenv("WORKERS_PER_CORE", 1))
NUM_WORKERS = int(os.getenv("NUM_WORKERS", WORKER_PER_CORE * NUM_CORES))
HOST = os.getenv("HOST", "0.0.0.0")
PORT = os.getenv("PORT", "8000")
ADDRESS = os.getenv("BIND", f"{HOST}:{PORT}")
KEEP_ALIVE = 120

assert NUM_WORKERS > 0

# Gunicorn config variables
backlog = 2048
workers = NUM_WORKERS
worker_connections = 1000
bind = ADDRESS
keepalive = KEEP_ALIVE
timeout = 30
keepalive = 2

debug = False
spew = False

errorlog = "-"
loglevel = "info"
accesslog = "-"


def post_fork(server, worker):
    server.log.info("Worker spawned (pid: %s)", worker.pid)


def pre_fork(server, worker):
    pass


def pre_exec(server):
    server.log.info("Forked child, re-executing.")


def when_ready(server):
    server.log.info("Server is ready. Spwawning workers")
