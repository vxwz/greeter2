from fastapi.testclient import TestClient
from greeter2 import __version__
from greeter2 import api

client = TestClient(api.get_app())


def test_version():
    assert __version__ == "0.1.0"


def test_greeter():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello, World!"}
